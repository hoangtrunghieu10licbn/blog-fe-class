import React, { Component } from 'react';
import { Layout } from 'antd';
import { Switch, Route, Redirect } from 'react-router-dom';
import QueueAnim from 'rc-queue-anim';
import routes from 'routes/app.routes';
import Header from './Header';

const { Content, Footer } = Layout;

class Home extends Component {
  render() {
    return (
      <QueueAnim type={['right', 'left']}>
        <div key="0">
          <Layout>
            <Header />
            <Content>
              <Switch>
                {routes.map((route, index) => (
                  <Route key={index} {...route}></Route>
                ))}
                <Route exact path="/" render={() => <Redirect to="/home" />} />
              </Switch>
            </Content>
            <Footer />
          </Layout>
        </div>
      </QueueAnim>
    );
  }
}

export default Home;
