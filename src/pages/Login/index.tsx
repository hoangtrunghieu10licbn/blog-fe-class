import React, { FunctionComponent } from 'react';
import QueueAnim from 'rc-queue-anim';
import Login from './components/Login';

const Page: FunctionComponent = () => {
  return (
    <QueueAnim type={['right', 'left']}>
      <div key="0">
        <Login />
      </div>
    </QueueAnim>
  );
};

export default Page;
