import React, { Component } from 'react';
import { Form, Button, Icon, Input, notification } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { validatePassword } from 'utils/validator';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Cookies } from 'react-cookie';
import login from 'services/Login';

const cookies = new Cookies();

interface Props extends FormComponentProps, RouteComponentProps {}

interface State {
  confirmDirty: boolean;
}

class Register extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      confirmDirty: false,
    };
  }

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { validateFieldsAndScroll } = this.props.form;
    validateFieldsAndScroll(async (error: any, fieldValues: any) => {
      if (!error) {
        console.log(`field values`, fieldValues);

        try {
          const result = await login({
            email: fieldValues.email,
            password: fieldValues.password,
          });

          console.log(`login resut`, result);

          if (result.err !== 0) {
            notification.error({
              message: 'Đăng nhập thất bại',
              description: `${result.msg}`,
            });
            return;
          }

          // just use cookies or local storage
          cookies.set('accessToken', result.data.access_token);
          cookies.set('userInfo', result.data.user);

          localStorage.setItem('accessToken', result.data.access_token);
          localStorage.setItem('userInfo', JSON.stringify(result.data.user));

          notification.success({
            message: `Đăng nhập thành công`,
            description: `${result.msg}`,
          });

          this.props.history.push('/home');
        } catch (error) {
          notification.error({
            message: 'Đăng nhập thất bại',
            description: `${error.toString()}`,
          });
        }
      }
    });
  };

  validateToPassword = (rule: any, value: any, callback: any) => {
    if (value && !validatePassword(value)) {
      callback(
        'The input is not valid Password! (8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character)',
      );
    }
    callback();
  };

  componentDidMount() {
    if (cookies.get('userInfo')) {
      this.props.history.push('/home');
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [
              { required: true, message: 'Please input your email!' },
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                max: 50,
                message: 'Character number is greater than allowed',
              },
            ],
          })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />)}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [
              { required: true, message: 'Please input your Password!' },
              {
                validator: this.validateToPassword,
              },
            ],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>
          Or <a href="/register">register now!</a>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedFormRegister = Form.create<FormComponentProps>({ name: 'register-form' })(withRouter(Register));

export default WrappedFormRegister;
