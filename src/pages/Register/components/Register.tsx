import React, { Component } from 'react';
import { Row, Col } from 'antd';
import RegisterForm from './RegisterForm';
import './Register.css';

class Register extends Component {
  constructor(props: {}) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <h1 className="title">Đăng ký</h1>
        <Row type="flex" justify="center" className="form-block">
          <Col sm={22} md={20} lg={12} xl={10}>
            <RegisterForm />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Register;
