import React, { Component } from 'react';
import { Form, Button, Icon, Input, notification, Radio, Tooltip } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { validatePassword } from 'utils/validator';
import register from 'services/Register';
import { Cookies } from 'react-cookie';
import { parsePhoneNumberFromString, isValidNumberForRegion } from 'libphonenumber-js';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import './Register.css';

const cookies = new Cookies();

const formItemLayout = {
  labelCol: {
    sm: { span: 24 },
    md: { span: 8 },
  },
  wrapperCol: {
    sm: { span: 24 },
    md: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    sm: {
      span: 24,
      offset: 0,
    },
    md: {
      span: 16,
      offset: 8,
    },
  },
};

enum USER_SEX {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
}

interface Props extends FormComponentProps, RouteComponentProps {}

interface State {
  confirmDirty: boolean;
}

class Register extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      confirmDirty: false,
    };
  }

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { validateFieldsAndScroll } = this.props.form;
    validateFieldsAndScroll(async (error: any, fieldValues: any) => {
      if (!error) {
        console.log(`field values`, fieldValues);

        const phoneNumber = parsePhoneNumberFromString(fieldValues.phoneNumber, 'VN');

        try {
          const result = await register({
            address: fieldValues.address,
            email: fieldValues.email,
            name: fieldValues.name,
            password: fieldValues.password,
            phone_number: phoneNumber ? phoneNumber.number.toString() : undefined,
            sex: fieldValues.sex,
          });

          console.log(`register result`, result);

          if (result.err !== 0) {
            notification.error({
              message: 'Đăng ký thất bại',
              description: `${result.msg}`,
            });
          }

          notification.success({
            message: `Đăng ký thành công`,
            description: `${result.msg}`,
          });

          this.props.history.push('/login');
        } catch (error) {
          notification.error({
            message: 'Đăng ký thất bại!',
            description: `${error.toString()}`,
          });
        }
      }
    });
  };

  handleConfirmBlur = (e: any) => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule: any, value: any, callback: any) => {
    const { getFieldValue } = this.props.form;
    if (value && value !== getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule: any, value: any, callback: any) => {
    const { validateFields } = this.props.form;
    if (value && this.state.confirmDirty) {
      validateFields(['confirm'], { force: true });
    }
    callback();
  };

  validateToPassword = (rule: any, value: any, callback: any) => {
    if (value && !validatePassword(value)) {
      callback(
        'The input is not valid Password! (8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character)',
      );
    }
    callback();
  };

  validateToPhoneNumber = (rule: any, value: any, callback: any) => {
    if (value && !isValidNumberForRegion(value, 'VN')) {
      callback('The input is not valid Phone Number');
    }
    callback();
  };

  componentDidMount() {
    if (cookies.get('userInfo')) {
      this.props.history.push('/home');
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
              {
                max: 50,
                message: 'Character number is greater than allowed!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Password" hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: 'Please input your password!',
              },
              {
                validator: this.validateToNextPassword,
              },
              {
                validator: this.validateToPassword,
              },
            ],
          })(<Input.Password />)}
        </Form.Item>
        <Form.Item label="Confirm Password" hasFeedback>
          {getFieldDecorator('confirm', {
            rules: [
              {
                required: true,
                message: 'Please confirm your password!',
              },
              {
                validator: this.compareToFirstPassword,
              },
            ],
          })(<Input.Password onBlur={this.handleConfirmBlur} />)}
        </Form.Item>
        <Form.Item
          label={
            <span>
              Name&nbsp;
              <Tooltip title="What do you want others to call you?">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          }
        >
          {getFieldDecorator('name', {
            rules: [
              { required: true, whitespace: true, message: 'Please input your name!' },
              {
                max: 50,
                message: 'Character number is greater than allowed!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Phone Number">
          {getFieldDecorator('phoneNumber', {
            rules: [
              {
                validator: this.validateToPhoneNumber,
              },
              {
                max: 50,
                message: 'Character number is greater than allowed!',
              },
            ],
          })(<Input style={{ width: '100%' }} />)}
        </Form.Item>
        <Form.Item label="Sex">
          {getFieldDecorator('sex', {
            rules: [],
          })(
            <Radio.Group>
              <Radio value={USER_SEX.MALE}> Male</Radio>
              <Radio value={USER_SEX.FEMALE}>Female</Radio>
            </Radio.Group>,
          )}
        </Form.Item>
        <Form.Item label="Address">
          {getFieldDecorator('address', {
            rules: [
              {
                max: 50,
                message: 'Character number is greater than allowed!',
              },
            ],
          })(<Input style={{ width: '100%' }} />)}
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedFormRegister = Form.create<FormComponentProps>({ name: 'register-form' })(withRouter(Register));

export default WrappedFormRegister;
