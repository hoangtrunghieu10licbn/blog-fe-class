import React, { FunctionComponent } from 'react';
import { List, Avatar, Icon } from 'antd';
import { Author } from 'types';
import { RouteComponentProps, withRouter, Link } from 'react-router-dom';

interface IconTextProps {
  type: string;
  text: string;
}

const IconText: FunctionComponent<IconTextProps> = props => {
  const { type, text } = props;

  return (
    <span>
      <Icon type={type} style={{ marginRight: 8 }} />
      {text}
    </span>
  );
};

interface PostItemProps extends RouteComponentProps {
  id: string;
  title: string;
  content: string;
  author: Author;
  image: string;
  className?: string;
}

const PostItem: FunctionComponent<PostItemProps> = props => {
  const { id, title, content, author, image, className } = props;

  return (
    <List.Item
      className={className}
      key={id}
      actions={[
        <IconText type="like-o" text="156" key="list-vertical-like-o" />,
        <IconText type="message" text="2" key="list-vertical-message" />,
      ]}
      extra={
        <img
          width={272}
          height={150}
          alt="logo"
          src={`${
            image
              ? `http://127.0.0.1:4000${image}`
              : `https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png`
          }`}
        />
      }
    >
      <List.Item.Meta
        avatar={
          <Avatar
            src={`${
              author.avatar ? author.avatar : `https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png`
            }`}
          />
        }
        title={
          <Link to={`/post-detail/${id}`}>
            <h2 className="post-item-title">{`${title}`}</h2>
          </Link>
        }
        description={`Author: ${author.name}`}
      />
      {content.length < 100 ? (
        <div>{`${content}`}</div>
      ) : (
        <div className="post-item-content">
          {`${content.slice(0, 99)}`}
          <Link to={`/post-detail/${id}`}>{`...read more`}</Link>
        </div>
      )}
    </List.Item>
  );
};

export default withRouter(PostItem);
