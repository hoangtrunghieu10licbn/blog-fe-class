import React, { Component } from 'react';
import QueueAnim from 'rc-queue-anim';
import PostList from './components/PostList';

class Page extends Component {
  render() {
    return (
      <QueueAnim type={['right', 'left']}>
        <div key="0">
          <PostList />
        </div>
      </QueueAnim>
    );
  }
}

export default Page;
