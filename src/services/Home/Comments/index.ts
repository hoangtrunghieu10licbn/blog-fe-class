import getCommentList from './getCommentList';
import addComment from './addComment';
import getCommentDetail from './getCommentDetail';
import removeComment from './removeComment';
import updateComment from './updateComment';

export { getCommentDetail, getCommentList, addComment, removeComment, updateComment };
