import addPost from './addPost';
import getPostDetail from './getPostDetail';
import getPostList from './getPostList';
import removePost from './removePost';
import updatePost from './updatePost';

export { addPost, getPostDetail, getPostList, removePost, updatePost };
