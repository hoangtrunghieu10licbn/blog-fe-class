import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Cookies } from 'react-cookie';
import { Post } from 'types';

const cookies = new Cookies();

export interface RequestData {
  _id: string;
  title?: string;
  content?: string;
  image_urls?: string[];
}

interface Request extends AxiosRequestConfig {
  data: RequestData;
}

interface ResponseData {
  err: number;
  msg: string;
  data: Data;
}

interface Data {
  post: Post;
}

const updatePost = async function(args: RequestData): Promise<ResponseData> {
  try {
    const accessToken: string = cookies.get('accessToken') || '';

    const request: Request = {
      method: 'PATCH',
      url: `${SERVER_URI}/api/post`,
      data: {
        ...args,
      },
      headers: {
        access_token: accessToken,
      },
    };

    const result: AxiosResponse<ResponseData> = await makeHttpRequest(request);

    return result.data;
  } catch (e) {
    return e;
  }
};
export default updatePost;
